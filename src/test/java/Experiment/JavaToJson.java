package Experiment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaToJson {
	
	public static void main(String[] args) throws JsonProcessingException {
		
		Employee employee = new Employee();
		employee.setFirstname("Sachin");
		employee.setLastname("Tendulkar");
		
		//Java object to Json object
		
		ObjectMapper mapper = new ObjectMapper();
		
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(employee));
		
		//Json Object to Java Object
		
		String JsonObject = "{\r\n" + 
				"  \"firstname\" : \"Sachin\",\r\n" + 
				"  \"lastname\" : \"Tendulkar\"\r\n" + 
				"}";
		Employee emp = mapper.readValue(JsonObject, Employee.class);
		
		System.out.println(emp.getFirstname());
		
	}
	

}
