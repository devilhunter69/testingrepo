package JsonNodeExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExtractingValueFromJsonArray {
	
	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		
		String json = "[\r\n" + 
				"{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"mobile\":[\"555-888-9999\",\"222-888-7777\"],\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"},\r\n" + 
				"{\r\n" + 
				"    \"firstname\" : \"Dave\",\r\n" + 
				"    \"lastname\" : \"Down\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"mobile\":[\"555-888-9999\",\"222-888-7777\"],\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}\r\n" + 
				"]";
		
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonAsJsonNode =  mapper.readTree(json);
		
		System.out.println(jsonAsJsonNode.get(1).get("firstname").asText());
		
	}
	
	
	
	
	

}
