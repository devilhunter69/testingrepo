package JsonNodeExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ArrayNodeExample {
	
	public static void main(String[] args) throws JsonProcessingException {
	
		ObjectMapper mapper = new ObjectMapper();
		
		ObjectNode payload =  mapper.createObjectNode();
		
		ArrayNode arrayMobile = mapper.createArrayNode();
		arrayMobile.add("555-888-9999");
		arrayMobile.add("222-888-7777");
		
		ObjectNode bookingDates =  mapper.createObjectNode();
		bookingDates.put("checkin", "2018-01-01");
		bookingDates.put("checkout", "2019-01-01");

		payload.put("firstname", "Jim");
		payload.put("lastname", "Brown");
		payload.put("totalprice", 1000);
		payload.put("depositpaid", true);
		payload.set("mobile", arrayMobile);
		payload.put("additionalneeds", "Lunch");
		payload.set("bookingdates", bookingDates);
		
		ObjectNode payload2 =  mapper.createObjectNode();
		
		ArrayNode arrayMobile2 = mapper.createArrayNode();
		arrayMobile2.add("555-888-9999");
		arrayMobile2.add("222-888-7777");
		
		ObjectNode bookingDates2 =  mapper.createObjectNode();
		bookingDates2.put("checkin", "2018-01-01");
		bookingDates2.put("checkout", "2019-01-01");

		payload2.put("firstname", "Jim");
		payload2.put("lastname", "Brown");
		payload2.put("totalprice", 1000);
		payload2.put("depositpaid", true);
		payload2.set("mobile", arrayMobile2);
		payload2.put("additionalneeds", "Lunch");
		payload2.set("bookingdates", bookingDates2);
		
		ArrayNode finalPayload = mapper.createArrayNode();
		finalPayload.add(payload);
		finalPayload.add(payload2);
		
		
		String createBookingPayload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(finalPayload);
		System.out.println(createBookingPayload);
		
		
		
		
	}
	
	
	

}
