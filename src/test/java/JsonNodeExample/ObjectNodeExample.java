package JsonNodeExample;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ObjectNodeExample {
	
	public static void main(String[] args) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode payload =  mapper.createObjectNode();
		
		ArrayNode arrayMobile = mapper.createArrayNode();
		arrayMobile.add("555-888-9999");
		arrayMobile.add("222-888-7777");
		
		ObjectNode bookingDates =  mapper.createObjectNode();
		bookingDates.put("checkin", "2018-01-01");
		bookingDates.put("checkout", "2019-01-01");
		

		
		
		payload.put("firstname", "Jim");
		payload.put("lastname", "Brown");
		payload.put("totalprice", 1000);
		payload.put("depositpaid", true);
		payload.set("mobile", arrayMobile);
		payload.put("additionalneeds", "Lunch");
		payload.set("bookingdates", bookingDates);
		

		
		String CreateBookingPayload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload);
		System.out.println(CreateBookingPayload);

	}
	
}
