package JsonNodeExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class EditJsonOnTheFly {

	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		
		
		String json = "{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" +  
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}";
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode editJson =  mapper.readValue(json, ObjectNode.class);

		
		ObjectNode bookingDates =  mapper.createObjectNode();
		bookingDates.put("checkin", "2000-01-01");
		bookingDates.put("checkout", "2019-01-01");
		
		ArrayNode arrayMobile = mapper.createArrayNode();
		arrayMobile.add("555-888-9999");
		arrayMobile.add("222-888-7777");
		
		editJson.put("mobile", "558855");
		editJson.put("lastname", "Down");
		editJson.set("mobile", arrayMobile);
		editJson.set("bookingdates", bookingDates);
		
		
		
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(editJson));
		


	}

}
