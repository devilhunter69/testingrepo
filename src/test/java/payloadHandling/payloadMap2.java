package payloadHandling;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class payloadMap2 {
	

	@Test
	public void payloadMap() {
		Map<String, Object> workVisa1 = new LinkedHashMap<>();
		workVisa1.put("US", false);
		workVisa1.put("UK", true);

		Map<String, Object> addMap1 = new LinkedHashMap<>();
		addMap1.put("street", "4 hanover");
		addMap1.put("city", "Brampton");
		addMap1.put("state", "ON");
		addMap1.put("workvisa", workVisa1);

		Map<String, Object> emp1 = new LinkedHashMap<>();
		emp1.put("name", "Viru");
		emp1.put("sub", "Cricket");
		emp1.put("address", addMap1);
		
		
		Map<String, Object> workVisa2 = new LinkedHashMap<>();
		workVisa2.put("US", false);
		workVisa2.put("UK", true);

		Map<String, Object> addMap2 = new LinkedHashMap<>();
		addMap2.put("street", "2 hanover");
		addMap2.put("city", "Surrey");
		addMap2.put("state", "BC");
		addMap2.put("workvisa", workVisa2);
		
		List<String> mobile = new ArrayList<>();
		mobile.add("555-888-8890");
		mobile.add("555-888-8899");

		Map<String, Object> emp2 = new LinkedHashMap<>();
		emp2.put("name", "rancho");
		emp2.put("sub", "science");
		emp2.put("address", addMap2);
		emp2.put("mobile", mobile);
		
		List<Map<String,Object>> allEmps = new ArrayList<>();
		allEmps.add(emp1);
		allEmps.add(emp2);
		
		

		RestAssured
			.given().
				log().
				all().
				contentType(ContentType.JSON).
				baseUri("https://restful-booker.herokuapp.com/")
				.body(allEmps).when().post("booking")
			.then()
				.log()
				.all()
				.statusCode(200);

	}


}
