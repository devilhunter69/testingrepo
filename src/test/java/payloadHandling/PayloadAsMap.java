package payloadHandling;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PayloadAsMap {

	@Test
	public void payloadMap() {
		Map<String, Object> workVisa = new LinkedHashMap<>();
		workVisa.put("US", false);
		workVisa.put("UK", true);

		Map<String, Object> addMap = new LinkedHashMap<>();
		addMap.put("street", "4 hanover");
		addMap.put("city", "Brampton");
		addMap.put("state", "ON");
		addMap.put("workvisa", workVisa);

		Map<String, Object> jsonObject = new LinkedHashMap<>();
		jsonObject.put("name", "Viru");
		jsonObject.put("sub", "Cricket");
		jsonObject.put("address", addMap);

		RestAssured
			.given().
				log().
				all().
				contentType(ContentType.JSON).
				baseUri("https://restful-booker.herokuapp.com/")
				.body(jsonObject).when().post("booking")
			.then()
				.log()
				.all()
				.statusCode(200);

	}

}
