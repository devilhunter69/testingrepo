package payloadHandling;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;

public class TypeRefExample2 {
	
	@Test
	public void getAllBookings() { 
	List<Map<String,Object>> allBookings = RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")	
		.when()
			.get("booking")	
		.then()
			.log()
			.all()
			.extract()
			.as(new TypeRef<List<Map<String,Object>>>(){});
	
		System.out.println(allBookings.size());
	
		for(Map<String,Object> booking : allBookings){
			System.out.println("Booking ID:" + booking.get("bookingid"));
		}

	
	}

}
