package pojoExample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonIncludeExample {
	
	public static void main(String[] args) {
		
		CreateBookingPayload2 createBookingPayload = new CreateBookingPayload2();
		
		createBookingPayload.setFirstname("Yuvraj");
		createBookingPayload.setLastname("Singh");
		
		ObjectMapper mapper = new ObjectMapper();
		String payload=null;
		try {
			payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(createBookingPayload);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(payload);
	}

}
