package pojoExample;

import java.util.LinkedList;
import java.util.List;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PayLoadUsingPojo3 {
	
	@Test
	public void creatBooking() { 
		
		List<Object> phone = new LinkedList<>();
		phone.add("555-888-9999");
		phone.add("222-888-7777");
		
		CreateBookingPayload pl1 = new CreateBookingPayload();
		pl1.setFirstname("Rahul");
		pl1.setLastname("Dravid");
		pl1.setTotalprice(111);
		pl1.setDepositpaid(true);
		pl1.setMobile(phone);
		pl1.setAdditionalneeds("Coffee");

		
		BookingDates bd1 = new BookingDates();
		bd1.setCheckin("2018-01-01");
		bd1.setCheckout("2019-01-01");
		pl1.setBookingdates(bd1);
		
		CreateBookingPayload pl2 = new CreateBookingPayload();
		pl2.setFirstname("Dave");
		pl2.setLastname("Down");
		pl2.setTotalprice(150);
		pl2.setDepositpaid(true);
		pl2.setAdditionalneeds("tea");
		
		BookingDates bd2 = new BookingDates();
		bd2.setCheckin("2018-01-01");
		bd2.setCheckout("2019-01-01");
		pl2.setBookingdates(bd2);
		
		
		List<CreateBookingPayload> listofPass = new LinkedList<>();
		
		listofPass.add(pl1);
		listofPass.add(pl2);
		
		
		
		
		
		ObjectMapper mapper = new ObjectMapper();
		String payload=null;
		try {
			payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listofPass);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(payload);
		
/*	RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
			
		.when()
			.post("booking")
			
		.then()
			.log()
			.all()
			.statusCode(200);*/
		
		
	}


}
