package pojoExample;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PayLoadUsingPojo {
	
	@Test
	public void creatBooking() { 
		
		CreateBookingPayload pl = new CreateBookingPayload();
		pl.setFirstname("Rahul");
		pl.setLastname("Dravid");
		pl.setTotalprice(150);
		pl.setDepositpaid(true);
		pl.setAdditionalneeds("tea");
		
		BookingDates bd = new BookingDates();
		bd.setCheckin("2018-01-01");
		bd.setCheckout("2019-01-01");
		pl.setBookingdates(bd);
		
		
		
		
		
		
		
		
		ObjectMapper mapper = new ObjectMapper();
		String payload=null;
		try {
			payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pl);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(payload);
		
/*	RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
				"    \"firstname\" : \"Jim\",\r\n" + 
				"    \"lastname\" : \"Brown\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2018-01-01\",\r\n" + 
				"        \"checkout\" : \"2019-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
			
		.when()
			.post("booking")
			
		.then()
			.log()
			.all()
			.statusCode(200);*/
		
		
	}

}
