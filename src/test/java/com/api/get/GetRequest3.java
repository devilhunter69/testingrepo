package com.api.get;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest3 {
	
	@Test
	public void restAssuredGet() {
			RestAssured.get("https://restful-booker.herokuapp.com/booking/1")
				.then()
				.statusCode(200)
				.statusLine("HTTP/1.1 200 OK")
				.body("firstname", Matchers.equalToIgnoringCase("jim"))
				.body("bookingdates.checkin", Matchers.equalTo("2018-11-22"));		

	}

}
