package com.api.get;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class SampleTestCases {

	//how to send a get request
	
	@Test
	public void sendGetRequest() {
		
		//https://reqres.in/api/users?page=2
		
		Response response = get("https://reqres.in/api/users?page=2");
		response.prettyPrint();
		System.out.println("status code in response "+ response.statusCode());
		System.out.println("response time: "+response.getTime());
	}
	
	// how to send a post request using rest-assured
	
	@Test
	public void sendPostRequest() {
		//https://reqres.in/api/users
		
		Response response = given().contentType(ContentType.JSON).body("C:\\Selenium_Workplace\\APITesting\\NewUser.json").post("https://reqres.in/api/users");
		response.prettyPrint();
		
		
	}
	
	// how to send a put request
	
	// how to send a delete request 
	
	
	
}
