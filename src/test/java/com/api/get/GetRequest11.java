package com.api.get;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest11 {
	
	@Test
	public void restAssuredGet() {
		Response response =	RestAssured.get("https://restful-booker.herokuapp.com/booking");
				
		response.prettyPrint();
		
		int size = response.jsonPath().getList("$").size();
		
		
		int bookingId = response.jsonPath().getInt("[0].bookingid");
		
		System.out.println(bookingId);
		System.out.println("total no of bookings " +size);

	}

}
