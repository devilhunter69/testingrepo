package com.api.get;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class GetRequest8 {
	
	@Test
	public void restAssuredGet() {
			RestAssured.get("https://restful-booker.herokuapp.com/booking/1")
				.then()
				.time(Matchers.lessThanOrEqualTo(2000L));
			
		Response response =	RestAssured.get("https://restful-booker.herokuapp.com/booking/1");
		long l = response.getTime();
		

	}
		
	
}
