package com.api.get;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class GetRequest5 {
	
	@Test
	public void restAssuredGet() {
		String firstname = 	RestAssured.get("https://restful-booker.herokuapp.com/booking/1")
				.then()
				.statusCode(200)
				.statusLine("HTTP/1.1 200 OK")
				.body("firstname", Matchers.equalToIgnoringCase("Mary"))
				.body("bookingdates.checkin", Matchers.equalTo("2019-06-21"))
				.extract()
				.jsonPath()
				.getString("firstname");
		
		System.out.println(firstname);

	}

}
