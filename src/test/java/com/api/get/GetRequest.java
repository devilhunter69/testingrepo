package com.api.get;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest {
	
	@Test
	public void restAssuredGet() {
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/1");
		String body = response.asString();
		int statCode = response.getStatusCode();
		String statLine = response.getStatusLine();
		System.out.println(statLine);

	}

}
