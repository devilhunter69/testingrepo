package com.api.get;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest4 {
	
	@Test
	public void restAssuredGet() {
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/1");
		
		System.out.println(response.jsonPath().get("firstname").toString());
		
		System.out.println(response.jsonPath().getString("firstname"));
		
		Object obj = response.jsonPath().get("firstname");
		if(obj instanceof String)
			System.out.println(obj.toString());
		else if (obj instanceof Integer) {
			int i = (Integer)obj;
			System.out.println(i);
		}
	}

}
