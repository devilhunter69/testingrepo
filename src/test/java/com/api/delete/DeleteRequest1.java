package com.api.delete;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DeleteRequest1 {
	
	@Test
	public void generateToken(ITestContext dataManager) {
		String token = RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
					"    \"username\" : \"admin\",\r\n" + 
					"    \"password\" : \"password123\"\r\n" + 
					"}")
			
		.when()
			.post("auth")
			
		.then()
			.log()
			.all()
			.statusCode(200)
			.extract()
			.jsonPath()
			.getString("token");
		
		dataManager.setAttribute("token", token);
		
	}
	
	@Test(dependsOnMethods="generateToken")
	public void deleteBooking(ITestContext dataManager) { 
	RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.header("Cookie", "token="+dataManager.getAttribute("token"))
			.pathParam("bookingid", 1)
			.baseUri("https://restful-booker.herokuapp.com/")	
		.when()
			.delete("booking/{bookingid}")
		.then()
			.log()
			.all()
			.statusCode(201);
		
		
	}

}
