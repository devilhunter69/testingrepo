package com.api.put;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PutRequest3 {
	
	@Test
	public void generateToken(ITestContext dataManager) {
		String token = RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
					"    \"username\" : \"admin\",\r\n" + 
					"    \"password\" : \"password123\"\r\n" + 
					"}")
			
		.when()
			.post("auth")
			
		.then()
			.log()
			.all()
			.statusCode(200)
			.extract()
			.jsonPath()
			.getString("token");
		
		dataManager.setAttribute("token", token);
		
	}
	
	@Test(dependsOnMethods="generateToken")
	public void updateBooking(ITestContext dataManager) { 
	RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.header("Cookie", "token="+dataManager.getAttribute("token"))
			.pathParam("bookingid", 1)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
				"    \"firstname\" : \"Dave\",\r\n" + 
				"    \"lastname\" : \"Down\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2022-01-01\",\r\n" + 
				"        \"checkout\" : \"2012-01-01\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
			
		.when()
			.put("booking/{bookingid}")
			
		.then()
			.log()
			.all()
			.statusCode(200);
		
		
	}


}
