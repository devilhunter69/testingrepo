package PojoResponse;

import java.util.List;



public class CreateBookingResponsePojo {
	
	private int bookingid;
	private CreateBookingPayload booking;
	
	public int getBookingid() {
		return bookingid;
	}
	public void setBookingid(int bookingid) {
		this.bookingid = bookingid;
	}
	public CreateBookingPayload getBooking() {
		return booking;
	}
	public void setBooking(CreateBookingPayload booking) {
		this.booking = booking;
	}
	

}
